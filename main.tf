resource "aws_vpc" "pedro_vpc" {
  cidr_block           = "10.0.0.0/16"
  enable_dns_hostnames = true
  enable_dns_support   = true

  tags = {
    Name = "terraform"
  }
}

resource "aws_subnet" "pedro_public_subnet" {
  vpc_id                  = aws_vpc.pedro_vpc.id
  cidr_block              = "10.0.1.0/24"
  map_public_ip_on_launch = true
  availability_zone       = "us-east-1a"

  tags = {
    Name = "terraform_public"
  }
}

resource "aws_internet_gateway" "pedro_igw" {
  vpc_id = aws_vpc.pedro_vpc.id

  tags = {
    Name = "terra-IGW"
  }
}

resource "aws_route_table" "pedro_public_rt" {
  vpc_id = aws_vpc.pedro_vpc.id


  tags = {
    Name = "Terraform_public_rt"
  }
}

resource "aws_route" "default_route" {
  route_table_id         = aws_route_table.pedro_public_rt.id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_internet_gateway.pedro_igw.id
}

resource "aws_route_table_association" "pedro_public_assoc" {
  subnet_id      = aws_subnet.pedro_public_subnet.id
  route_table_id = aws_route_table.pedro_public_rt.id
}

resource "aws_security_group" "pedro_sg" {
  name        = "Terra_sg"
  description = "Security group created from terraform"
  vpc_id      = aws_vpc.pedro_vpc.id

  ingress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }
}

resource "aws_key_pair" "pedro_ssh_key_pair" {
  key_name   = "terra_key"
  public_key = file("~/.ssh/terra_key.pub")
}

resource "aws_instance" "pedro_ec2" {
  ami                    = data.aws_ami.pedro-server-ami.id
  instance_type          = "t2.micro"
  key_name               = aws_key_pair.pedro_ssh_key_pair.id
  vpc_security_group_ids = [aws_security_group.pedro_sg.id]
  subnet_id              = aws_subnet.pedro_public_subnet.id
  user_data              = file("userdata.tpl")

  root_block_device {
    volume_size = 10
  }

  tags = {
    Name = "terra_instance"
  }
}