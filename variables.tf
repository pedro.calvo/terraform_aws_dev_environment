variable "host_os" {
  type    = string
  default = "ubuntu"
}

#variable actually not used, just to give the example of hoe to declare
# in order to reference it the syntax is ${var-name} e.g ${host_os}