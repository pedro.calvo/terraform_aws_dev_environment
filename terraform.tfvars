host_os = "fedora"



# in order to reference it the syntax is ${var-name} e.g ${host_os}

#terraform.tfvars overrides every declared and referenced variable 
#there  a couple of ways in order to use a different variable,
#different from the on declared on terraform.tfvars

#1.declaring a different one on the cli, e.g: terraform console -var="host_os=unix"
#2. referencing another file with a different value, on the console e.g: -var-file="dev.tfvars"
